#include <Wire.h>

#include "i2c_scan.h"


int i2c_scan_for_devices(TwoWire *i2c_bus, i2c_scan_for_devices_cb *callback)
{
  int nDevices = 0;
  uint8_t address;

  for(address = 1; address < 127; address++) {
    uint8_t error;
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    i2c_bus->beginTransmission(address);
    error = i2c_bus->endTransmission();

    if (error == 0) {
      if (callback) {
        (*callback)(address);
      }
      nDevices++;
    } else if (error == 4) {
//      Serial.print("Unknown error at address 0x");
    }    
  }
  return nDevices;
}
