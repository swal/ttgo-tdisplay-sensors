#ifndef I2C_SCAN_H_
#define I2C_SCAN_H_

#include <stdbool.h>
#include <stdint.h>

/* https://github.com/esp8266/Arduino/blob/master/libraries/Wire/Wire.h */
#include <Wire.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef bool i2c_scan_for_devices_cb(uint8_t address);

extern int i2c_scan_for_devices(TwoWire *i2c_bus, i2c_scan_for_devices_cb *callback);

#ifdef __cplusplus
}
#endif

#endif
