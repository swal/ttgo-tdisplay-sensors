/*
   Sketchbook: P:\src\ArduinoIDE-sketchbook\t-display-bmp280
   Boards: https://dl.espressif.com/dl/package_esp32_index.json

      Werkzeuge - Board:   ESP32 Dev Module
*/

#include <Arduino.h>

#include "tft_tdisplay.h"
#include <SPI.h>
#include "WiFi.h"
#include <Wire.h>
#include "i2c_scan.h"

#include "Adafruit_BME680.h"

/**
   Measurement interval in milliseconds.
*/
#define INTERVAL_MS 1000

#define SDA_1 21
#define SCL_1 22
#define I2C_HZ_1 10000

#define SDA_2 32
#define SCL_2 33
#define I2C_HZ_2 10000

typedef struct I2C_bus {
  uint8_t instance_number;
  uint8_t sda_pin;
  uint8_t scl_pin;
  unsigned long bus_hz; /**< I2C bus clocj in Hz */
  TwoWire twowire;

  void begin(void) {
    twowire.begin(sda_pin, scl_pin, bus_hz);
  };
} i2c_bus_t;

i2c_bus_t i2c_bus[] = {
  { 0, SDA_1, SCL_1, I2C_HZ_1, TwoWire(0)},
  { 1, SDA_2, SCL_2, I2C_HZ_2, TwoWire(1)},
};

TFT_TDisplay tft;

#define BME680_COUNT (4)

Adafruit_BME680 *(bme680[BME680_COUNT]);
bool bme680_available[BME680_COUNT];

typedef struct I2C_connection {
  i2c_bus_t *bus;
  uint8_t address;
} i2c_connection_t;

i2c_connection_t bme680_connection[BME680_COUNT] = {
  {&i2c_bus[0], 0x76},
  {&i2c_bus[0], 0x77},
  {&i2c_bus[1], 0x76},
  {&i2c_bus[1], 0x77}
};

char device_name[32];

bool on_i2c_scan_for_devices(uint8_t address)
{
  Serial.printf("I2C device on addr 0x%02X", address);
  Serial.println();

  char buffer[32];
  snprintf(buffer, sizeof(buffer), " %02X", address);
  tft.print(buffer);

  return false;
}

void setup()
{
  Serial.begin(115200);

  /* Print some device ids and firmware ids to the serial port. */
  uint64_t chipid = ESP.getEfuseMac(); /* The chip ID is essentially its MAC address(length: 6 bytes). */
  snprintf(device_name, sizeof(device_name), "DEV_ID: %0" PRIX64, chipid);

  Serial.printf(__DATE__ " " __TIME__);
  Serial.println();
  Serial.printf(device_name);
  Serial.println();

  /* Setup the TTGO T-Display's display and
     print device ids and firmware ids as well.
  */
  tft.setup();
  tft.println(__DATE__ " " __TIME__);
  tft.println(device_name);

  /* Currently, we do not support WiFi */
  WiFi.mode(WIFI_OFF);

  /* Scan all configured I2C busses */
  for (unsigned int i_bus = 0; i_bus < sizeof(i2c_bus) / sizeof(i2c_bus[0]); i_bus++) {
    Serial.printf("I2C_B%i SDA=%i SCL=%i", i_bus, i2c_bus[i_bus].sda_pin, i2c_bus[i_bus].scl_pin);
    Serial.println();
    i2c_bus[i_bus].begin();

    Serial.printf("I2C_B%i scanning...", i_bus);
    Serial.println();

    char buffer[32];
    snprintf(buffer, sizeof(buffer), "I2C_B%i:", i_bus);
    tft.print(buffer);

    int nof_devices = i2c_scan_for_devices(&i2c_bus[i_bus].twowire, &on_i2c_scan_for_devices);
    Serial.printf("I2C_B%i: %i devices", i_bus, nof_devices);
    Serial.println();

    snprintf(buffer, sizeof(buffer), " #%i", nof_devices);
    tft.println(buffer);
  }

  /* Wait, so the user can read what we printed. */
  vTaskDelay(4000 / portTICK_PERIOD_MS);

  for (unsigned int i = 0; i < BME680_COUNT; i++) {
    bme680[i] = new Adafruit_BME680(&(bme680_connection[i].bus->twowire));

    Serial.printf("BME680[%u] begin (SDA=%i SCL=%i ADDR=0x%02X) ...", i, bme680_connection[i].bus->sda_pin, bme680_connection[i].bus->scl_pin, bme680_connection[i].address);
    if (bme680[i]->begin(bme680_connection[i].address)) {
      Serial.println("OK");
      bme680_available[i] = true;

      // Set up oversampling and filter initialization
      bme680[i]->setTemperatureOversampling(BME680_OS_8X);
      bme680[i]->setHumidityOversampling(BME680_OS_2X);
      bme680[i]->setPressureOversampling(BME680_OS_4X);
      bme680[i]->setIIRFilterSize(BME680_FILTER_SIZE_3);
      bme680[i]->setGasHeater(320, 150); // 320*C for 150 ms
    } else {
      Serial.println("Failed");
    }
  }
}


unsigned long interval_next_ms = 0;

void interval_delay(unsigned long interval_ms)
{
  unsigned long now_ms = millis();
  unsigned long delay_ms = interval_next_ms - now_ms;

  //  Serial.printf("interval_delay: %11lu  ", delay_ms);

  if (delay_ms > interval_ms) {
    /* We are late!
       Better wait only less than interval_ms
    */
    //    Serial.printf("interval_delay: jump");
    delay_ms = interval_ms / 4;
    interval_next_ms = now_ms + interval_ms;
  } else {
    interval_next_ms += interval_ms;
  }

  delay(delay_ms);
}


void reset_bme680_values(Adafruit_BME680 *bme680)
{
  bme680->temperature = 0.0;
  bme680->humidity = 0.0;
  bme680->pressure = 0;
  bme680->gas_resistance = 0.0;
}

void loop(void)
{
  unsigned long now_ms = millis();

  for (unsigned int i = 0; i < BME680_COUNT; i++) {
    reset_bme680_values(bme680[i]);

    if (bme680_available[i]) {
      unsigned long endTime = bme680[i]->beginReading();
      if (endTime == 0) {
        // Serial.println(F("Failed to begin reading :("));
        reset_bme680_values(bme680[i]);
      } else {
        if (!bme680[i]->endReading()) {
          // Serial.println(F("Failed to complete reading :("));
          reset_bme680_values(bme680[i]);
        } else {
        }
      }
    }
  }


  Serial.printf("%11lu", now_ms);

  for (unsigned int i = 0; i < BME680_COUNT; i++) {
    Serial.printf(",%8.3f,%7.3f,%9.3f,%11.3f", bme680[i]->temperature, bme680[i]->humidity, bme680[i]->pressure / 100.0, bme680[i]->gas_resistance / 1.0);
  }
  Serial.println();


  tft.clear();
  /* Display width is 20 characters, in small font */
  char buffer[32];

  snprintf(buffer, sizeof(buffer), "Sec: %8.3f\n", now_ms / 1000.0);
  tft.println(buffer);

  for (unsigned int i = 0; i < BME680_COUNT; i++) {
    snprintf(buffer, sizeof(buffer), "%u: %5.1f %4.1f %6.1f\n", i, bme680[i]->temperature, bme680[i]->humidity, bme680[i]->pressure / 100.0 /*, bme680.gas_resistance / 1.0*/);
    tft.println(buffer);
  }

  interval_delay(1000);
}
