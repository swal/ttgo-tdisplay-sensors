/*
 * The TFT_eSPI library needs to be adapted to the display/device type.
 * For the TTGO T-Display cange the following lines in the file User_Setup_Select.h
 * 
 * Disable / remove the line:
 * #include <User_Setup.h>           // Default setup is root library folder
 * 
 * Enable / activate the line:
 * #include <User_Setups/Setup25_TTGO_T_Display.h>    // Setup file for ESP32 and TTGO T-Display ST7789V SPI bus TFT
 */

#include <TFT_eSPI.h> /* https://github.com/Bodmer/TFT_eSPI/blob/fb2e669d37c05d5f6eef4275816b8c68476233a3/TFT_eSPI.h */

// #define TFT_WIDTH  135
// #define TFT_HEIGHT 240

class TFT_TDisplay
{
  public:
    TFT_TDisplay();
    void setup(void);

    void clear();
    void print(const char *msg);
    void println(const char *msg);

    TFT_eSPI tft;

  private:
    int16_t x0;
    int16_t y0;
};
