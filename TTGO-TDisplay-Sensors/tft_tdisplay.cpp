
#include "tft_tdisplay.h"


#ifndef TFT_DISPOFF
#define TFT_DISPOFF     0x28
#endif

#ifndef TFT_SLPIN
#define TFT_SLPIN       0x10
#endif

/* used gpio-pins */
#define TFT_MOSI        19
#define TFT_SCLK        18
#define TFT_CS           5
#define TFT_DC          16
#define TFT_RST         23

#define TFT_BL           4  // Display backlight control pin

static TFT_eSPI tft = TFT_eSPI(TFT_WIDTH, TFT_HEIGHT); // Invoke custom library


TFT_TDisplay::TFT_TDisplay()
{
  y0 = 0;
}

void TFT_TDisplay::setup(void)
{
  tft.init();
  tft.setRotation(1);
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_WHITE);
  tft.setCursor(0, 0);
  tft.setTextDatum(TL_DATUM);  /* text allignment: MC_DATUM = Middle centre */
  tft.setTextSize(2);

  if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User_Setups file Setup25_TTGO_T_Display.h
    pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
    digitalWrite(TFT_BL, TFT_BACKLIGHT_ON); // Turn backlight on. TFT_BACKLIGHT_ON has been set in the TFT_eSPI library in the User_Setup file Setup_TTGO_T_Display.h
  }

  tft.setSwapBytes(true);

  x0 = 1;
  y0 = 1;
}

void TFT_TDisplay::clear(void)
{
  x0 = 1;
  y0 = 1;
  
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_WHITE, TFT_BLACK); /* set text foureground and background color */
  tft.setCursor(0, 0);
  tft.setTextDatum(TL_DATUM); /* text allignment */
  tft.setTextSize(2);
}

void TFT_TDisplay::print(const char *msg)
{
  tft.drawString(msg, x0, y0);
  x0 += tft.textWidth(msg);
}

void TFT_TDisplay::println(const char *msg)
{
  tft.drawString(msg, x0, y0);
  x0 = 1;
  y0 += tft.fontHeight() * 3 / 2;
}
