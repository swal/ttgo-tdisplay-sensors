# Environmental Sensors Connected to TTGO T-Display

## About

## Sensors
Included sensors are
* BME280
* BME680
* MS8607-02BA01
* SHT21

Copies of the data sheets are in the `/extra/` folder this repository.

### BME280

### BME680
https://www.bosch-sensortec.com/products/environmental-sensors/gas-sensors/bme680
https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bme680-ds001.pdf
https://www.mouser.de/datasheet/2/783/bst_bme680_sf000-2486508.pdf
https://www.mouser.de/ProductDetail/Bosch-Sensortec/BME680-Shuttle-Board-30?qs=Wj%2FVkw3K%252BMABg5lm5143Ww%3D%3D

### MS8607-02BA01
### SHT21

## I2C Bus

Be aware that some of these sensors use the identical I2C addresses, which makes it impossible to use them on the same I2C bus.

| Sensor Device | I2C Addresses |
| ------ | ------ |
| BME280 |`0x76` __or__ `0x77`, configurable by pin A0 |
| BME680 |`0x76` __or__ `0x77`, configurable by pin A0 |
| MS8607 |`0x40` __and__ `0x76`|
| SHT21  |`0x40`|

To deal with that, two independent I2C-busses are used.

| Index | Bus | SDA Pin | SCL Pin | Connected Device |
| ---   | --- | ---     | ---     | --- |
|     0 |  1  | 21      | 22 | BME280 @`0x76`, BME680 @`0x77`, SHT21 @`0x40`|
|     1 |  2  | 32      | 33 | MS8607 @`0x40`and `0x76` |

# Development Environment
## USB-UART Driver
*TTGO T-Display* modules come with different types of USB-to-UART interface ICs, depending on when/where you bought them.

The popular operating systems typically have the corresponding driver already installed. But in a few cases this might not be the case.

Recent *TTGO T-Display* modules likely come with a *CH9102* USB-to-UART IC.

### USB-SERIAL CH9102

In the case of the *CH9102*, a matching driver was not installed on my Windows PC and could not be found online by the OS.

Following the recommendation from https://arduino.stackexchange.com/questions/88522/drivers-for-ch9102x-serial-port-chip I downloaded the driver from http://www.wch.cn/downloads/CH343CDC_ZIP.html. For conveniance it is included in this repository as well (in folder `/extra/`)

Other drivers I tried before did not work at all or had severe stability issues (blue screen on shutdown).

## Arduino IDE

Download and install the Arduino IDE (https://www.arduino.cc/en/software). I have no experience with version 2.0.0, so I would recommend the Legacy Arduino IDE 1.8.19


## The Source Code

This repository includes copies of all needed libraries in the `/libraries/` folder.

* "Download source code". The file will be named like ttgo-tdisplay-sensors-main.zip
* unzip the archive to a new folder (e.g. ttgo-tdisplay-sensors-main)

* Start the `Arduino` application
* Open the dialog from `File` - `Prefenences`
** Set the `Sketchbook location` folder to the folder ttgo-tdisplay-sensors-main you created
** It not already included in the `Additional Boards Manager URLs` add `https://dl.espressif.com/dl/package_esp32_index.json` (separated by comma).
* Open the "Board Manager" from the menu `Tools` - `Board: SOMETHING` - `Boards Manager...`
** Type `esp32` into the text field to filter the list
** Install `esp32` by **Espressif Systems**
* From `Tools` - `Board: SOMETHING` - `ESP32 Arduino` select `ESP32 Dev Module`
* From `File`- `Open` ttgo-tdisplay-sensors-main\TTGO-TDisplay-Sensors\TTGO-TDisplay-Sensors.ino
** A new `Arduino` window may open. You can close the old one.
* Compile the project from `Sketch` - `Verify/Compile` (or press the leftmost button in the toolbar)
** Compilation my take a few minutes.

Upload the program (the "sketch" to the TTGO T-Display module
* From the menu `Tool` - `Port` select the serial port the T-Display is connected to
* Upload the sketch by pressing the second button (showing an arrow to the right) in the toolbar
** This may take a minute as well.
* The T-Display will automatically start the new sketch after upload finished.

You can open the debug terminal by pressing the rightmost button in the tool bar (maginifying glass)
* set to 115200 baud

## Data Acquisition

The device's serial connection is configured to use 115200 Baud.

Data can be received with any serial console program.

### SerialPlot
A very recommended application running in Windows is *SerialPlot* (https://github.com/hyOzd/serialplot). A Settigns file for *SerialPlot* matching this project is included in the `/extra/` folder.
